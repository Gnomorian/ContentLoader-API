/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. 
 */

package nz.co.crookedhill.clapi;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

import com.google.common.collect.ImmutableSet;
import com.google.common.reflect.ClassPath;
import com.google.common.reflect.ClassPath.ClassInfo;


/**
 * this class is a utility for loading objects into 
 * your minecraft mod without the mod having to know 
 * where it is.
 * 
 * it is recommended that you only use it if you have 
 * allot of items or entities as this may cause a small 
 * performance hit, but only as much as mods hit 
 * themselves already.
 * 
 * @author William Cameron (Gnomorian)
 * @email  w.cameron@crookedhill.co.nz
 * @Website http://www.crookedhill.co.nz/
 */
public class CLAPI 
{
	public class item
	{
		/**
		 * returns a hashmap of items found within a package.
		 * however no arguments are passed to the object as it 
		 * is expected that the item will do it all itself 
		 * e.g. set its own creative tab.
		 * 
		 * @param packagename e.g. "nz.co.crookedhill.items"
		 * @return hashmap of item object and their name as the key, e.g. <"hotdog",ItemHotDog.class>
		 */
		public HashMap<String, Item>getItems(String packagename)
		{
			HashMap<String, Item> items = new HashMap<String, Item>();
			ImmutableSet<ClassInfo> set;
			try {
				set = ClassPath.from(this.getClass().getClassLoader()).getTopLevelClassesRecursive(packagename);
				System.out.println("the sit is " + set.size() + "long.");
				for(ClassInfo cinfo : set)
				{
					Class<?> clas = Class.forName(cinfo.getName());
					Constructor<?> constr = clas.getConstructor();
					Item item = (Item) constr.newInstance();
					items.put(item.getUnlocalizedName().replace("item.", ""), item);
				}
				System.out.printf("%s items loaded out of %s files found in the package %s", items.size(), set.size(), packagename);
			} catch (IOException e) {
				System.out.println("Error with loading list of objects in the package \"" + packagename + "\" you provided.");
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				System.out.println("Error with loading Class within the package you proveded, it may not be a valid class.");
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				System.out.println("Error with passing arguments to a class, the constructor shoudl be: public ClassName(CreativeTabs creativetab){}");
				e.printStackTrace();
			} catch (SecurityException e) {
				System.out.println("Error with passing arguments to a class, is the constructor public?");
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				System.out.println("Error with instantiating the class, is the class public?");
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
			
			return items;
		}
		
		public HashMap<String, Item> getItems(String packagename, CreativeTabs ctab)
		{
			HashMap<String, Item> items = new HashMap<String, Item>();
			ImmutableSet<ClassInfo> set;
			try {
				set = ClassPath.from(this.getClass().getClassLoader()).getTopLevelClassesRecursive(packagename);
				System.out.println("the sit is " + set.size() + "long.");
				for(ClassInfo cinfo : set)
				{
					Class<?> clas = Class.forName(cinfo.getName());
					Constructor<?> constr = clas.getConstructor(CreativeTabs.class);
					Item item = (Item) constr.newInstance(ctab);
					items.put(item.getUnlocalizedName().replace("item.", ""), item);
				}
				System.out.printf("%s items loaded out of %s files found in the package %s", items.size(), set.size(), packagename);
			} catch (IOException e) {
				System.out.println("Error with loading list of objects in the package \"" + packagename + "\" you provided.");
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				System.out.println("Error with loading Class within the package you proveded, it may not be a valid class.");
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				System.out.println("Error with passing arguments to a class, the constructor shoudl be: public ClassName(CreativeTabs creativetab){}");
				e.printStackTrace();
			} catch (SecurityException e) {
				System.out.println("Error with passing arguments to a class, is the constructor public?");
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				System.out.println("Error with instantiating the class, is the class public?");
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
			
			return items;
		}
	}
	public class entity
	{
		
	}
}
